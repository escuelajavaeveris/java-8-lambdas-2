import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by hgranjal on 07/06/2018.
 */
public class Main {

    public static void main(String[] args) {
        Transaction transaction1 = new Transaction(1500, "EUR");
        Transaction transaction2 = new Transaction(1000, "EUR");
        Transaction transaction3 = new Transaction(10, "EUR");
        Transaction transaction4 = new Transaction(5, "EUR");
        List<Transaction> transactionList = Arrays.asList(transaction1, transaction2, transaction3, transaction4);

        TransactionsFilter transactionsFilter = new TransactionsFilter();
        List<Transaction> bigTransactionsList = transactionsFilter.filterBigTransactions(transactionList);
        List<Transaction> smallTransactionsList = transactionsFilter.filterSmallTransactions(transactionList);

        //Predicate<Transaction> predicate = t -> t.getAmount() > 10;
    }
}
