import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

/**
 * Created by hgranjal on 13/06/2018.
 */
public class TransactionsFilter {

    public List<Transaction> filterTransactions(List<Transaction> transactions, Predicate<Transaction> transactionPredicate) {
        List<Transaction> result = new ArrayList<>();

        for (Transaction transaction: transactions) {
            if (transactionPredicate.test(transaction)) {
                result.add(transaction);
            }
        }

        return result;
    }

    public List<Transaction> filterBigTransactions(List<Transaction> transactions) {
        return filterTransactions(transactions, t -> t.getAmount() > 1000);
    }

    public List<Transaction> filterSmallTransactions(List<Transaction> transactions) {
        return filterTransactions(transactions, t -> t.getAmount() < 10);
    }
}
